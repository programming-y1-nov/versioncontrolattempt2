class Player {
  PVector p1,p2,p3;
  float speed=4;
  float bulletMax = 5, shootingCooldown, shootingCooldownMax = 0.1;
  ArrayList<Bullet> bullets;
  
  Player() {
    bullets = new ArrayList<Bullet>();
    
    p1 = new PVector(250, 700);
    p2 = new PVector(230, 750);
    p3 = new PVector(270, 750);
  }
  
}
